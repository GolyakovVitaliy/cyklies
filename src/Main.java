public class Main {
    public static void main(String[] args) {
//1 задача
        Zadacha1 zadacha1 = new Zadacha1();
        String s1 = zadacha1.zad1(0,0);
        System.out.println("1-е задание = " + s1);

        //2 задача
        Zadacha2 zadacha2 = new Zadacha2();
        String s2 = zadacha2.zad2(17);
        System.out.println("2-е задание = " + s2);

//4 задача
        Zadacha4 zadacha4 = new Zadacha4();
        int s4 = zadacha4.zad4(7);
        System.out.println("4-е задание = " + s4);

        //5 задача
        Zadacha5 zadacha5 = new Zadacha5();
        int s5 = zadacha5.zad5(1223);
        System.out.println("5-е задание = " + s5);

        //6 задача
        Zadacha6 zadacha6 = new Zadacha6();
        String s6 = zadacha6.zad6(49000);
        System.out.println("6-е задание = " + s6);
    }
}
