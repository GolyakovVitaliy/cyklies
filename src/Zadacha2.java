//2 задача
public class Zadacha2 {
    public String zad2(int n) {

        int x = 0;

        if (n < 2) x = 2;
        else {
            double s = Math.sqrt(n);
            for (int i = 2; i <= s; i++) {
                if (n % i == 0)
                    x = 1;
                else x = 0;
            }

        }
        if (x == 0) return ("Простое");
        else return("Не простое");

    }
}
